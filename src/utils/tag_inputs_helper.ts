export default {
  checkKeywords(value: any, maxCount: any) {
    // check if the total sum of characters for keywords is <= 1000
    let count = 0;
    if (typeof value === "string") {
      count = value.length;
    } else if (value !== undefined) {
      for (const keyword of value) {
        count += keyword.length;
      }
    }
    return count <= maxCount;
  },
  parseKeywords(form: any, delimiter = ";") {
    // combine array values to a string (using the 'keyword'  form input)
    if (typeof form.Keywords !== "string") {
      let keywordString = "";
      for (const keyword of form.Keywords) {
        keywordString += keyword + delimiter;
      }
      if (keywordString.charAt(keywordString.length - 1) === delimiter) {
        keywordString = keywordString.substring(0, keywordString.length - 1);
      }
      form.Keywords = keywordString;
    }
  },
};
