declare type CoscineType = {
  language: {
    locale: string;
  };
  i18n: {
    "project-creation": VueI18n.LocaleMessages | undefined;
  };
};

declare const coscine: CoscineType;

declare interface Window {
  coscine: CoscineType;
}

declare let _spPageContextInfo: any;

declare class Organization {
  public url: string;
  public displayName: string;
}
